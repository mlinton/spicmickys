<?php //
/**
 * Spicmickys functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Spicmickys
 */

// Initilize the Theme
add_action( 'wp_enqueue_scripts', 'spicmickys_enqueue_styles' );
add_action( 'after_setup_theme', 'spicmickys_setup_theme', 20 );

/*
 * Init Styles
 */
function spicmickys_enqueue_styles() {
    $theme = wp_get_theme();
    
    // Load parent theme styles and scripts
    wp_enqueue_style( 'bootstrap', get_template_directory_uri()  . '/css/bootstrap.css', 
        array(),  // if the parent theme code has a dependency, copy it to here
        $theme->parent()->get('Version')
    );
    wp_enqueue_style( 'gloriafood-style', get_template_directory_uri() . '/style.css', 
        array(),  // if the parent theme code has a dependency, copy it to here
        $theme->parent()->get('Version')
    );
    
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.js', 
        array('jquery'),  // if the parent theme code has a dependency, copy it to here
        $theme->parent()->get('Version')
    );
    
    wp_enqueue_style( 'gloriafood--print-style', 'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i|PT+Serif:400,400i,700,700i', 
        array(),  // if the parent theme code has a dependency, copy it to here
        $theme->parent()->get('Version')
    );
    
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    if ( gloriafood_can_generate_order_buttons_html() && 1 === get_theme_mod( 'glf_order_buttons_show_buttons', 0 ) ) {
        wp_enqueue_script( 'gloriafood-order-buttons', get_template_directory_uri() . '/js/gloriafood-order-buttons.js', 
                array( 'jquery' ), $theme->parent()->get('Version'), true );
    }
        
    // Load Spicmickys styles and scripts
    wp_enqueue_style( 'spicmickys-style', get_stylesheet_uri(),
        array( 'gloriafood-style' ),
        $theme->get('Version') // this only works if you have Version in the style header
    );
    wp_enqueue_style( 'spicmickys-CelticaBoldOblique', 'https://fontlibrary.org//face/celtica',
        array( 'gloriafood-style' ),
        $theme->get('Version') // this only works if you have Version in the style header
    );
}

/*
 * 
 */
function spicmickys_setup_theme() {
    add_theme_support(
            'custom-logo',
            array(
                'height'      => 100,
                'width'       => 300,
                'flex-width'  => true,
                'flex-height' => true,
            ));
}

?>