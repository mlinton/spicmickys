=== Spicmicky's ===

Contributors: Matthew Linton
Tags: custom-background, custom-logo, custom-menu, featured-images, threaded-comments, translation-ready

Requires at least: 5.0
Tested up to: 5.4
Requires PHP: 5.6
Stable tag: 1.0.0
License: GNU General Public License v3 or later
License URI: https://www.gnu.org/licenses/old-licenses/gpl-3.0.en.html

Spicmicky's is a child theme for Gloriafood Restaurant created for Spicmicky's Bar & Restaurant

== Description ==

Long Description

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==


== Changelog ==

= 1.0 - May 12 2015 =
* Initial release
